import os
import cv2
import numpy as np
import utils_config as config
# Leer archivos de configuración
config_file = "C:\\Users\\Christian\\Desktop\\taller\\SuperResolution\\configs\\srgan.config"
config = config.load_config(config_file)


def get_train_data():
    X = []
    Y = []
    for x in os.listdir(config.DB_DIR + "\\train\\lr"):
        img_x = cv2.imread(config.DB_DIR + "\\train\\lr\\" + x)
        X.append(img_x)

    X = np.array(X) / 255

    for y in os.listdir(config.DB_DIR + "\\train\\hr"):
        img_y = cv2.imread(config.DB_DIR + "\\train\\hr\\" + y)
        Y.append(img_y)

    Y = np.array(Y) / 255
    return X, Y


def get_test_data():
    X = []
    Y = []

    for x in os.listdir(config.DB_DIR + "\\test\\lr"):
        img_x = cv2.imread(config.DB_DIR + "\\test\\lr\\" + x)
        X.append(img_x)

    X = np.array(X) / 255

    for y in os.listdir(config.DB_DIR + "\\test\\hr"):

        img_y = cv2.imread(config.DB_DIR + "\\test\\hr\\" + y)
        Y.append(img_y)
    Y = np.array(Y) / 255

    return X, Y
