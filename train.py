import os
import cv2
import numpy as np
import time
import matplotlib.pyplot as plt
import utils_config as config
from tqdm import tqdm, trange

from tensorflow.keras.models import Sequential
from tensorflow.keras import Model
from tensorflow.keras.layers import Conv2D, PReLU, BatchNormalization, UpSampling2D, LeakyReLU, Dense, Input, add, Flatten


from utils import get_train_data, get_test_data
from srgan_models.generator import create_gen
from srgan_models.discriminator import create_disc
from srgan_models.vgg19 import build_vgg
from srgan_models.main import create_comb

id_gpu = '0'
os.environ['CUDA_VISIBLES_DEVICES'] = id_gpu

# Leer archivos de configuración
config_file = "C:\\Users\\Christian\\Desktop\\taller\\SuperResolution\\configs\\srgan.config"
config = config.load_config(config_file)

# Cargar datos de entrenamiento
train_lr, train_hr = get_train_data()
print("train_shapeHR ", train_hr.shape, " train_shapeLR ", train_lr.shape)


#
hr_shape = (train_hr.shape[1], train_hr.shape[2], train_hr.shape[3])
lr_shape = (train_lr.shape[1], train_lr.shape[2], train_lr.shape[3])

lr_ip = Input(shape=lr_shape)
hr_ip = Input(shape=hr_shape)

# Crear y compilar modelos

generator = create_gen(lr_ip)
discriminator = create_disc(hr_ip)
discriminator.compile(loss="binary_crossentropy",
                      optimizer="adam", metrics=['accuracy'])

vgg = build_vgg(hr_shape)
vgg.trainable = True


gan_model = create_comb(generator, discriminator, vgg, lr_ip, hr_ip)
gan_model.compile(loss=["binary_crossentropy", "mse"],
                  loss_weights=[1e-3, 1], optimizer="adam")

gan_model.summary()

# Preparar entrenamiento
train_lr_batches = []
train_hr_batches = []
batch_size = config.BATCH_SIZE

for it in range(int(train_hr.shape[0] / batch_size)):
    start_idx = it * batch_size
    end_idx = start_idx + batch_size
    train_hr_batches.append(train_hr[start_idx:end_idx])
    train_lr_batches.append(train_lr[start_idx:end_idx])

train_lr_batches = np.array(train_lr_batches)
train_hr_batches = np.array(train_hr_batches)


# Continuar entrenamiento
if config.SNAPSHOT_CONTINUE == bool(True):
    generator.load_weights(config.SNAPSHOT_GEN +
                           str(config.EPOCH_START) + ".h5")
    discriminator.load_weights(
        config.SNAPSHOT_DIS + str(config.EPOCH_START) + ".h5")

# Iniciar entrenamiento

for e in range(config.EPOCH_START, config.EPOCH_END):
    gen_label = np.zeros((batch_size, 1))
    real_label = np.ones((batch_size, 1))
    g_losses = []
    d_losses = []
    start_time = time.time()
    lastB = 0

    dirFiles = tqdm(range(len(train_hr_batches)))

    for b in dirFiles:
        lastB = b
        lr_imgs = train_lr_batches[b]
        hr_imgs = train_hr_batches[b]

        gen_imgs = generator.predict_on_batch(lr_imgs)

        discriminator.trainable = True
        d_loss_gen = discriminator.train_on_batch(gen_imgs, gen_label)
        d_loss_real = discriminator.train_on_batch(hr_imgs, real_label)
        discriminator.trainable = False

        d_loss = 0.5 * np.add(d_loss_gen, d_loss_real)
        image_features = vgg.predict(hr_imgs)

        g_loss, _, _ = gan_model.train_on_batch(
            [lr_imgs, hr_imgs], [real_label, image_features])
        d_losses.append(d_loss)
        g_losses.append(g_loss)
        dirFiles.set_description(
            f"Procesando {b} de {len(train_hr_batches)}")

    g_losses = np.array(g_losses)
    d_losses = np.array(d_losses)

    cv2.imwrite(f"{config.SNAPSHOT_TRAIN_IMG}_{e+1}_gen.jpg", gen_imgs[1]*255)
    cv2.imwrite(f"{config.SNAPSHOT_TRAIN_IMG}_{e+1}_lr.jpg", lr_imgs[1]*255)
    cv2.imwrite(f"{config.SNAPSHOT_TRAIN_IMG}_{e+1}_hr.jpg", hr_imgs[1]*255)

    g_loss = np.sum(g_losses, axis=0) / len(g_losses)
    d_loss = np.sum(d_losses, axis=0) / len(d_losses)

    print("epoch:", e+1, "g_loss:", g_loss, "d_loss:", d_loss,
          "--- %s seconds ---" % (time.time() - start_time))

    if (e+1) % config.SNAPSHOT_STEPS == 0:
        discriminator.save_weights(config.SNAPSHOT_DIS + str(e+1) + ".h5")
        generator.save_weights(config.SNAPSHOT_GEN + str(e+1) + ".h5")
