import os
import cv2
import numpy as np
import time
import matplotlib.pyplot as plt
import utils_config as config
from tqdm import tqdm, trange

from tensorflow.keras.layers import Input


from utils import get_train_data, get_test_data
from srgan_models.generator import create_gen
from srgan_models.discriminator import create_disc
from srgan_models.vgg19 import build_vgg
from srgan_models.main import create_comb

id_gpu = '0'
os.environ['CUDA_VISIBLES_DEVICES'] = id_gpu

# Leer archivos de configuración
config_file = "C:\\Users\\Christian\\Desktop\\taller\\SuperResolution\\configs\\srgan.config"
config = config.load_config(config_file)

# Cargar datos de prueba
test_lr, test_hr = get_test_data()
print("test_shapeHR ", test_hr.shape, " test_shapeLR", test_lr.shape)

hr_shape = (test_hr.shape[1], test_hr.shape[2], test_hr.shape[3])
lr_shape = (test_lr.shape[1], test_lr.shape[2], test_lr.shape[3])

lr_ip = Input(shape=lr_shape)
hr_ip = Input(shape=hr_shape)

generator = create_gen(lr_ip)
discriminator = create_disc(hr_ip)

discriminator.compile(loss="binary_crossentropy",
                      optimizer="adam", metrics=['accuracy'])

vgg = build_vgg(hr_shape)
vgg.trainable = True

generator.load_weights(f"{config.SNAPSHOT_GEN}4.h5")
discriminator.load_weights(f"{config.SNAPSHOT_DIS}4.h5")


gan_model = create_comb(generator, discriminator, vgg, lr_ip, hr_ip)
gan_model.compile(loss=["binary_crossentropy", "mse"],
                  loss_weights=[1e-3, 1], optimizer="adam")

eval = gan_model.evaluate(
    [test_lr, test_hr], [np.ones((test_hr.shape[1], 1)), vgg.predict(test_hr)])

print(eval)

test_pred = generator.predict_on_batch(test_lr[:10])

plt.imshow(test_pred[1])
plt.show()


plt.imshow(test_lr[1])
plt.show()

plt.imshow(test_hr[1])
plt.show()
