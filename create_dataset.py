import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm, trange
import time
import random
import utils_config as config

# Leer archivos de configuración
config_file = "C:\\Users\\Christian\\Desktop\\taller\\SuperResolution\\configs\\srgan.config"
config = config.load_config(config_file)


def rotation(img, angle):
    angle = int(random.uniform(-angle, angle))
    h, w = img.shape[:2]
    M = cv2.getRotationMatrix2D((int(w/2), int(h/2)), angle, 1)
    img = cv2.warpAffine(img, M, (w, h))
    return img


def brightness(img, low, high):
    value = random.uniform(low, high)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    hsv = np.array(hsv, dtype=np.float64)
    hsv[:, :, 1] = hsv[:, :, 1]*value
    hsv[:, :, 1][hsv[:, :, 1] > 255] = 255
    hsv[:, :, 2] = hsv[:, :, 2]*value
    hsv[:, :, 2][hsv[:, :, 2] > 255] = 255
    hsv = np.array(hsv, dtype=np.uint8)
    img = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    return img


# Image Augmentation TRAIN
lenHr = sum([len(files) for r, d, files in os.walk(
    config.TRAIN_DIR + "\\original")])
indice = 0
dirFiles = tqdm(np.array(os.listdir))
for img in os.listdir(config.TRAIN_DIR + "\\original"):
    img_arr = cv2.imread(config.TRAIN_DIR + "\\original\\" + img)

    # Size
    hr_img_arr = cv2.resize(img_arr, (100, 100))
    lr_img_arr = cv2.resize(img_arr, (25, 25))
    cv2.imwrite(config.TRAIN_DIR + "\\hr\\" + img, hr_img_arr)
    cv2.imwrite(config.TRAIN_DIR + "\\lr\\" + img, lr_img_arr)

    # Rotate
    #hr_img_arr_rotate = rotation(hr_img_arr, 30)
    #lr_img_arr_rotate = cv2.resize(hr_img_arr_rotate, (35, 35))
    #cv2.imwrite(config.TRAIN_DIR + "\\hr\\" + "r_" + img, hr_img_arr_rotate)
    #cv2.imwrite(config.TRAIN_DIR + "\\lr\\" + "r_" + img, lr_img_arr_rotate)

    # Brightness
    #hr_img_arr_brightness = brightness(hr_img_arr, 0.5, 3)
    #lr_img_arr_brightness = cv2.resize(hr_img_arr_brightness, (35, 35))
    # cv2.imwrite(config.TRAIN_DIR + "\\hr\\" +
    #            "b_" + img, hr_img_arr_brightness)
    # cv2.imwrite(config.TRAIN_DIR + "\\lr\\" +
    #            "b_" + img, lr_img_arr_brightness)

    indice = indice + 1
    dirFiles.set_description(f"Procesando {indice} de {lenHr} : {img}")


# Image Augmentation TEST
lenHr = sum([len(files)
             for r, d, files in os.walk(config.TEST_DIR + "\\original")])
indice = 0
dirFiles = tqdm(np.array(os.listdir))
for img in os.listdir(config.TEST_DIR + "\\original"):
    img_arr = cv2.imread(config.TEST_DIR + "\\original\\" + img)

    # Size
    hr_img_arr = cv2.resize(img_arr, (100, 100))
    lr_img_arr = cv2.resize(img_arr, (25, 25))
    cv2.imwrite(config.TEST_DIR + "\\hr\\" + img, hr_img_arr)
    cv2.imwrite(config.TEST_DIR + "\\lr\\" + img, lr_img_arr)

    # Rotate
    #hr_img_arr_rotate = rotation(hr_img_arr, 30)
    #lr_img_arr_rotate = cv2.resize(hr_img_arr_rotate, (35, 35))
    #cv2.imwrite(config.TEST_DIR + "\\hr\\" + "r_" + img, hr_img_arr_rotate)
    #cv2.imwrite(config.TEST_DIR + "\\lr\\" + "r_" + img, lr_img_arr_rotate)

    # Brightness
    #hr_img_arr_brightness = brightness(hr_img_arr, 0.5, 3)
    #lr_img_arr_brightness = cv2.resize(hr_img_arr_brightness, (35, 35))
    #cv2.imwrite(config.TEST_DIR + "\\hr\\" + "b_" + img, hr_img_arr_brightness)
    #cv2.imwrite(config.TEST_DIR + "\\lr\\" + "b_" + img, lr_img_arr_brightness)

    indice = indice + 1
    dirFiles.set_description(f"Procesando {indice} de {lenHr} : {img}")
