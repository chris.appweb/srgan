# SRGAN

### Keywords

    GAN, SRGAN, VGG19, Keras, Transfer Learning

### Overview

Este es el código implementado en Keras API de python con GAN (Generative Adversial Network). En este programa, podemos mejorar la calidad de píxeles de la imagen de 25x25 a 100x100.

### Dataset

Conjunto de datos a partir de 1100 imágenes de 669x665 contenidas en las carpetas:

> ./dataset-lg/train/original

> ./dataset-lg/test/original

Los datos de entranamiento se generan al ejecutando el script "create_dataset.py". Ejemplo:

```
python create_dataset.py
```

Se crearán imagenes de alta resolucion (100x100) en la carpeta hr y baja resolucion (25x25) en la carpeta lr. Se utilizarán estas imagenes para entrenar nuestro Generador y Discriminador.

### Background

La referencia al trabajo de investigación de este programa se puede encontrar [aquí](https://arxiv.org/pdf/1609.04802). GAN es una clase de marcos de aprendizaje automático diseñados por Ian Goodfellow y sus colegas en 2014.

### Program

El archivo _train.py_ se usa para entrenar el modelo durante el paso inicial.

El archivo _predict.py_ se usa para evaluar el modelo.

El archivo _/configs/srgan.config_ se utiliza para ajustar parametros.

```
"NUM_EPOCHS": Número de épocas.

"BATCH_SIZE": Bloques de imagenes que pasamos a nuestro modelo en cada epoca.

"SNAPSHOT_STEPS": Cantidad de épocas que transcurren antes de guardar los modelos de entrenamiento del generador y discriminador.

"TRAIN_DIR": Directorio con datos de entrenamiento.

"TEST_DIR": Directorio con datos de evaluación.

"DB_DIR": Directorio base de nuestro dataset.

"SNAPSHOT_DIS": Directorio donde se guarda el entrenamiento del discriminador.

"SNAPSHOT_GEN": Directorio donde se guarda el entrenamiento del generador.

"SNAPSHOT_TRAIN_IMG": Directorio donde se guarda las imagenes de seguimiento durante el entrenamiento.

"NUM_RES_BLOCK": Número de bloques residuales.

"EPOCH_START": Época de inicio, en un inicio es 0, luego debemos poner la epoca desde donde queramos continuar cuando nuestro modelo ya está entrenado.

"EPOCH_END": Época final, al llegar a esta epoca el entrenamiento se detiene automáticamente.

"SNAPSHOT_CONTINUE": Se deja en false para iniciar un entrenamiento desde 0 o se utiliza true para continuar nuestro entramiento desde EPOCH_START en adelante.

```

Se utlizó el modelo VGG19 para extraer características de las imágenes de alta resolución durante el entrenamiento.

### References

Articulo guía para entender e iniciar la arquitectura SRGAN con keras [aquí](https://medium.com/@manishdhakal/super-resolution-with-gan-and-keras-srgan-4bd810d214b6)

Repositorio del articulo [aquí](https://github.com/manishdhakal/SuperResolution)

Paper on SRGAN [aquí](https://arxiv.org/pdf/1609.04802)
