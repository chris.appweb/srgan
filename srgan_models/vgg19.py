from tensorflow.keras.applications import VGG19
from tensorflow.keras import Model


def build_vgg(hr_shape):
    vgg = VGG19(weights="imagenet", include_top=False,
                input_shape=hr_shape)
    img_features = vgg(vgg.inputs)

    return Model(inputs=vgg.inputs, outputs=img_features)


# def build_vgg():
#    vgg = VGG19(weights="imagenet", include_top=False,
#                input_shape=(140, 140, 3))
#    img_features = vgg(vgg.inputs)
#
#    return Model(inputs=vgg.inputs, outputs=img_features)
