from tensorflow.keras import Model
from tensorflow.keras.layers import Conv2D, PReLU, BatchNormalization, UpSampling2D, LeakyReLU, Dense, Input, add, Flatten


def discriminator_block(ip, filters, strides=1, bn=True):

    disc_model = Conv2D(filters, (3, 3), strides=strides, padding="same")(ip)
    disc_model = LeakyReLU(alpha=0.2)(disc_model)
    if bn:
        disc_model = BatchNormalization(momentum=0.8)(disc_model)

    return disc_model


def create_disc(disc_ip):

    df = 64

    d1 = discriminator_block(disc_ip, df, bn=False)
    d2 = discriminator_block(d1, df, strides=2)
    d3 = discriminator_block(d2, df*2)
    d4 = discriminator_block(d3, df*2, strides=2)
    d5 = discriminator_block(d4, df*4)
    d6 = discriminator_block(d5, df*4, strides=2)
    d7 = discriminator_block(d6, df*8)
    d8 = discriminator_block(d7, df*8, strides=2)

    d8_5 = Flatten()(d8)
    d9 = Dense(df*16)(d8_5)
    d10 = LeakyReLU(alpha=0.2)(d9)
    validity = Dense(1, activation='sigmoid')(d10)

    return Model(disc_ip, validity)
